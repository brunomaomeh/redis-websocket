package org.brunomaomeh.listener;

import java.io.IOException;
import java.util.Set;

import javax.websocket.Session;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class RedisListener {

	protected static final String JEDIS_SERVER = "localhost";
	private Set<Session> sessions;

	public RedisListener(Set<Session> sessions) {
		this.sessions = sessions;
		setupSubscriber();
	}
	
	private JedisPubSub setupSubscriber() {
		final JedisPubSub jedisPubSub = new JedisPubSub() {
			@Override
			public void onUnsubscribe(String channel, int subscribedChannels) {
				System.out.println("onUnsubscribe");
			}

			@Override
			public void onSubscribe(String channel, int subscribedChannels) {
				System.out.println("onSubscribe");
			}

			@Override
			public void onPUnsubscribe(String pattern, int subscribedChannels) {
			}

			@Override
			public void onPSubscribe(String pattern, int subscribedChannels) {
			}

			@Override
			public void onPMessage(String pattern, String channel, String message) {
			}

			@Override
			public void onMessage(String channel, String message) {
//				messageContainer.add(message);
				System.out.println("Received TEXT message: " + message);
				for (Session sess: sessions) {
					System.out.println("Sending message to: " + sess.getId());
					try {
						sess.getBasicRemote().sendText(message.toUpperCase());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					System.out.println("Connecting");
					Jedis jedis = new Jedis(JEDIS_SERVER);
					System.out.println("subscribing");
					jedis.subscribe(jedisPubSub, "test");
					System.out.println("subscribe returned, closing down");
					jedis.quit();
				} catch (Exception e) {
					System.out.println(">>> OH NOES Sub - " + e.getMessage());
					// e.printStackTrace();
				}
			}
		}, "subscriberThread").start();
		return jedisPubSub;
	}

}
