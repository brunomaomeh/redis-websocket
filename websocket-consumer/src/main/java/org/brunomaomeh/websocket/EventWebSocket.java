package org.brunomaomeh.websocket;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.brunomaomeh.listener.RedisListener;

@ClientEndpoint
@ServerEndpoint(value = "/events")
public class EventWebSocket {

	private final static Set<Session> sessions = new HashSet<>();
	
	public RedisListener redisListener = new RedisListener(sessions);

	@OnOpen
	public void onWebSocketConnect(Session session) {
		sessions.add(session);
		System.out.println("Socket Connected: " + session.getId());
		System.out.println("Numero de sessoes: " + sessions.size());
	}

	@OnMessage
	public void onWebSocketText(String message) throws IOException {
		System.out.println("Received TEXT message: " + message);
		for (Session sess: sessions) {
			System.out.println("Sending message to: " + sess.getId());
			sess.getBasicRemote().sendText(message.toUpperCase());
		}
	}

	@OnClose
	public void onWebSocketClose(CloseReason reason, Session session) {
		sessions.remove(session);
		System.out.println("Socket Closed: " + reason);
	}

	@OnError
	public void onWebSocketError(Throwable cause, Session session) {
		sessions.remove(session);
		cause.printStackTrace(System.err);
	}
}