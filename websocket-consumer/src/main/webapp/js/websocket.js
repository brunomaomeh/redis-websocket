var ws = new WebSocket('ws://localhost:8080/websocket-consumer/events');

ws.onopen = function() {
	console.log('Conexão aberta com sucesso');
};

ws.onmessage = function(messageEvent) {
	console.log('chegou a mensagem: ' + messageEvent.data);
	angular.element("#widget-controller").scope().add(messageEvent.data);
};
