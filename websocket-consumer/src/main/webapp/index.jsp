<!DOCTYPE html>
<html ng-app="WidgetApp">
<head>
<meta charset="utf-8">
<title>Websocket Consumer</title>
<link rel="stylesheet" href="css-lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="css-lib/bootstrap/bootstrap-theme.css">

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="js-lib/bootstrap/bootstrap.min.js"></script> -->
<script type="text/javascript" src="js-lib/angular/angular.min.js"></script>
<script type="text/javascript" src="js-lib/angular/ui-bootstrap-tpls-0.13.3.min.js"></script>
<script type="text/javascript" src="js/websocket.js"></script>
<script type="text/javascript" src="js/widgetController.js"></script>
</head>
<body>
	<div id="header">
		<h3>Websocket Consumer</h3>
	</div>
	<div id="widget-controller" ng-controller="WidgetController as widgetController">
		<div ng-repeat="widget in todos">
			<p>{{widget.text}}</p>
		</div>
		<button type="button" class="btn btn-default" ng-click="open()">Open me!</button>
		<button type="button" class="btn btn-default" ng-click="open('sm')">Small modal</button>
		<script type="text/ng-template" id="myModalContent.html">
			<div class="modal-header">
				<h3 class="modal-title">I'm a modal!</h3>
			</div>
			<div class="modal-body">
				<div ng-repeat="widget in todos">
					<p>{{widget.text}}</p>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
				<button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
			</div>
		</script>
	</div>
</body>
</html>