package org.brunomaomeh.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.brunomaomeh.model.Mensagem;
import org.brunomaomeh.service.RedisService;

public class RedisServlet extends HttpServlet {

	private RedisService redisService;
	
	public RedisServlet() {
		System.out.println("Inicio Servlet");
		redisService = new RedisService();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer numeroDeThreads = new Integer(req.getParameter("numeroDeThreads"));
		Integer intervaloDeThreads = new Integer(req.getParameter("intervaloDeThreads"));
		Integer numeroDeMensagens = new Integer(req.getParameter("numeroDeMensagens"));
		String mensagem = req.getParameter("mensagem");
		
		System.out.println(mensagem);
		redisService.publicar(new Mensagem(numeroDeThreads, intervaloDeThreads, numeroDeMensagens, mensagem));
		resp.sendRedirect(req.getHeader("Referer"));
	}
	
}
