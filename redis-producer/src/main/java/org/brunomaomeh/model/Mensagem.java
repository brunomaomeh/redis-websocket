package org.brunomaomeh.model;

public class Mensagem {

	private Integer numeroDeThreads;
	private Integer intervaloDeThreads;
	private Integer numeroDeMensagens;
	private String mensagem;
	
	public Mensagem(Integer numeroDeThreads, Integer intervaloDeThreads, Integer numeroDeMensagens, String mensagem) {
		super();
		this.numeroDeThreads = numeroDeThreads;
		this.intervaloDeThreads = intervaloDeThreads;
		this.numeroDeMensagens = numeroDeMensagens;
		this.mensagem = mensagem;
	}

	public Integer getNumeroDeThreads() {
		return numeroDeThreads;
	}

	public Integer getIntervaloDeThreads() {
		return intervaloDeThreads;
	}

	public Integer getNumeroDeMensagens() {
		return numeroDeMensagens;
	}

	public String getMensagem() {
		return mensagem;
	}
	
}
