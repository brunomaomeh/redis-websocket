package org.brunomaomeh.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RedisController {
	
	@RequestMapping(value="/redis/index", method=RequestMethod.GET)
	public void index() {
		System.out.println("redis");
	}
	
	@RequestMapping(value="/redis/form", method=RequestMethod.POST)
	public void form() {
		System.out.println("recebeu");
	}

}
