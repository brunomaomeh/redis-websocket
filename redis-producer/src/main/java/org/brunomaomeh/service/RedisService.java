package org.brunomaomeh.service;

import org.brunomaomeh.model.Mensagem;

import redis.clients.jedis.Jedis;

public class RedisService {
	private static final String JEDIS_SERVER = "localhost";
	
	public void publicar(Mensagem mensagem) {
		try {
			System.out.println("Connecting");
			Jedis jedis = new Jedis(JEDIS_SERVER);
			System.out.println("Ready to publish, waiting one sec");
			Thread.sleep(1000);
			System.out.println("publishing");
			for (int i = 0; i < mensagem.getNumeroDeThreads(); i++) {
				final int iThread = i + 1;
				System.out.println("Iniciando Thread " + i);
				new Thread(new Runnable() {
					@Override
					public void run() {
						for (int j = 0; j < mensagem.getNumeroDeMensagens(); j++) {
							int iMensagem = j + 1;
							System.out.println("Publicando a mensagem " + iMensagem + " da Thread" + iThread);
							jedis.publish("test", mensagem.getMensagem() + " | Thread: " + iThread + " - Mensagem: " + iMensagem);
							try {
								System.out.println("Thread " + iThread + " sleeping for" + mensagem.getIntervaloDeThreads());
								Thread.sleep(mensagem.getIntervaloDeThreads());
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						System.out.println("Finalizando o envio de mensagens da Thread " + iThread);
						jedis.quit();
					}
				}).start();
			}
			System.out.println("published, closing publishing connection");
			System.out.println("publishing connection closed");
		} catch (Exception e) {
			System.out.println(">>> OH NOES Pub, " + e.getMessage());
		}
	}

}
