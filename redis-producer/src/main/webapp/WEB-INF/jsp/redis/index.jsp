<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>Redis Consumer</title>
</head>
<body>
	<h1>Redis Consumer</h1>
	<c:url var="url" value="/redis/form.jsp" /> 
	<form action="${url}" method="post">
		<fieldset>
			<div class="form-row">
				<label for="firstName">input:</label>
				<span class="input"><input name="id" /></span>
			</div>
			<div class="form-buttons">
				<div class="button">
					<input name="submit" type="submit" value="Send" />
				</div>
			</div>
		</fieldset>
	</form>
</body>
</html>