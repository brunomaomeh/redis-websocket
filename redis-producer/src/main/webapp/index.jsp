<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Redis Consumer</title>
</head>
<body>
	<div id="header">
		<h3>Redis Consumer</h3>
	</div>
	<div>
		<form action="/redis-producer/redisServlet">
			Numero de Threads: <input type="text" name="numeroDeThreads" value="2" /><br />
			Intervalo em milisegundos: <input type="text" name="intervaloDeThreads" value="3000" /><br />
			<br />
			Numero de Mensagens: <input type="text" name="numeroDeMensagens" value="10" /><br />
			Mensagem: <textarea rows="3" cols="30" name="mensagem"></textarea>
			<input type="submit" value="enviar" />
		</form>
	</div>
</body>
</html>